const fetch = require('node-fetch');
const crypto = require('crypto');


const {
  B2_APPLICATION_KEYID: applicationKeyId,
  B2_APPLICATION_KEY: applicationKey,
  B2_API_VERSION: apiVersion,
  B2_AUTH_URL: b2AuthUrl,
  B2_FILENAME_PREFIX: fileNamePrefix,
} = process.env;
const encodedBase64 = Buffer.from(`${applicationKeyId}:${applicationKey}`, 'utf8').toString('base64');
let authCreds = {};
let uploadCreds = {};

async function initialize() {
  const b2AuthOptions = {
    method: 'POST',
    headers: { Authorization: 'Basic ' + encodedBase64 },
    body: JSON.stringify({})
  };
  let response;

  try {
    response = await fetch(b2AuthUrl, b2AuthOptions);
  } catch (err) {
    throw new Error(`b2 initialize err: ${JSON.stringify(err)}`);
  }

  const {status, statusText, headers } = response;
  console.log('B2 Auth\n', b2AuthUrl, status);
  const data = await response.json();
  const { accountId, authorizationToken, allowed, apiUrl, downloadUrl } = data;
  const { bucketId, bucketName, capabilities, namePrefix } = allowed;

  console.info({ ...data });

  authCreds = { accountId, authorizationToken, allowed, apiUrl, downloadUrl };
  setTimeout(() => { getUploadUrl();}, 1000);
}

async function getUploadUrl() {
  const b2UploadUrl = `${authCreds.apiUrl}${apiVersion}/b2_get_upload_url`;
  const b2UploadOptions = {
    method: 'POST',
    headers: { Authorization: authCreds.authorizationToken },
    body: JSON.stringify({ bucketId: authCreds.allowed.bucketId })
  };
  let response;

  try {
    response = await fetch(b2UploadUrl, b2UploadOptions);
  } catch (err) {
    throw new Error(`b2 getUploadUrl err: ${JSON.stringify(err)}`);
  }

  const { status, statusText, headers } = response;
  console.log('getUploadUrl\n', b2UploadUrl, status);
  const data = await response.json();
  const { authorizationToken, bucketId, uploadUrl } = data;

  uploadCreds = { authorizationToken, bucketId, uploadUrl };
}

async function listFiles() {
  const b2ListFilesUrl = `${authCreds.apiUrl}${apiVersion}/b2_list_file_names`;
  const b2ListFilesOptions = {
    method: 'POST',
    headers: { Authorization: authCreds.authorizationToken },
    body: JSON.stringify({ bucketId: authCreds.allowed.bucketId, prefix: fileNamePrefix }),
  };
  let response;

  try {
    response = await fetch(b2ListFilesUrl, b2ListFilesOptions);
  } catch (err) {
    throw new Error(`b2 listFiles err: ${JSON.stringify(err)}`);
  }

  const { status, statusText, headers } = response;
  console.log('listFiles\n', b2ListFilesUrl, status);
  const data = await response.json();
  return data.files;
}

async function uploadFile(fName, fContents) {
  const b2UploadFileUrl = uploadCreds.uploadUrl;
  const b2UploadFileOptions = {
    method: 'POST',
    headers: {
      Authorization: uploadCreds.authorizationToken,
      'X-Bz-File-Name': encodeURI(fName),
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(fContents),
      'X-Bz-Content-Sha1': crypto.createHash('sha1').update(fContents, 'utf8').digest('hex'),
    },
    body: fContents
  }
  let response;

  try {
    response = await fetch(b2UploadFileUrl, b2UploadFileOptions);
  } catch (err) {
    throw new Error(`b2 uploadFile err: ${JSON.stringify(err)}`);
  }

  const { status, statusText, headers } = response;
  console.log('uploadFile\n', b2UploadFileUrl, status);
  const data = await response.json();
  const { accountId, action, bucketId, contentLength, contentMd5, contentSha1, contentType, fileId, fileInfo, fileName, uploadTimestamp } = data;

  return fileId;
}

// used for readinessProbe
const getAuthCreds = () => authCreds;


module.exports = {
  getAuthCreds,
  initialize,
  listFiles,
  uploadFile
};
