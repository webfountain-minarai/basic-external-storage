const uploadFile = require('../endpoints/backblaze').uploadFile;


const fileNamePrefix = process.env.B2_FILENAME_PREFIX;
let configNum = 3;

async function postFiles(req, res) {
  let reqBody = [];

  req.on('data', chunk => reqBody.push(chunk));
  req.on('end', async () => {
    reqBody = Buffer.concat(reqBody).toString();

    const fileName = `${fileNamePrefix}config_${configNum}.json`;
    const fileId = await uploadFile(fileName, reqBody);
    configNum++;

    const resBody = JSON.stringify({
      message: `file information: ${fileId}: ${fileName}`
    });

    res.writeHead(201, {
      'Content-Length': Buffer.byteLength(resBody),
      'Content-Type': 'application/json'
    })
    res.write(resBody);
    res.end();
  });
}


module.exports = postFiles;
